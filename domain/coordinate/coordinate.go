package coordinate

import (
	"database/sql"
	"time"
)

// Coordinate ...
type Coordinate struct {
	ID               int           `db:"id"`
	UserID           int           `db:"user_id"`
	BrandID          int           `db:"brand_id"`
	LabelID          int           `db:"label_id"`
	ShopID           int           `db:"shop_id"`
	MainCoordinateID int           `db:"main_coordinate_id"`
	CoordinateCode   string        `db:"coordinate_code"`
	ImageURL         string        `db:"image_url"`
	Comment          string        `db:"comment"`
	Height           int           `db:"height"`
	Gender           int           `db:"gender"`
	Accept           int           `db:"accept"`
	AcceptAt         time.Time     `db:"accept_at"`
	ApprovalAt       sql.NullTime  `db:"approval_at"`
	CreatedAt        time.Time     `db:"created_at"`
	UpdatedAt        time.Time     `db:"updated_at"`
	DeletedAt        sql.NullTime  `db:"deleted_at"`
	PV               int           `db:"pv"`
	Purchase         int           `db:"purchase"`
	IsLinked         int           `db:"is_linked"`
	AllowSync        sql.NullInt64 `db:"allow_sync"`
}
