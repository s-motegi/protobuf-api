package brand

import (
	"database/sql"
	"time"
)

// Brand ...
type Brand struct {
	ID             int            `db:"id"`
	BrandCode      sql.NullString `db:"brand_code"`
	Name           string         `db:"name"`
	NameKana       string         `db:"name_kana"`
	Zip            string         `db:"zip"`
	Address1       string         `db:"address1"`
	Address2       string         `db:"address2"`
	Address3       string         `db:"address3"`
	Staff          string         `db:"staff"`
	Email          string         `db:"email"`
	Tel            string         `db:"tel"`
	MerchantID     string         `db:"merchant_id"`
	TestMerchantID string         `db:"test_merchant_id"`
	StartAt        time.Time      `db:"start_at"`
	EndAt          time.Time      `db:"end_at"`
	CreatedAt      time.Time      `db:"created_at"`
	UpdatedAt      time.Time      `db:"updated_at"`
}

// ValidMerchantID is MerchantIDの正当性検証
func (brand *Brand) ValidMerchantID(merchantID string) bool {
	return merchantID == brand.MerchantID
}
