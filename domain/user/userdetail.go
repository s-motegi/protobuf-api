package user

import (
	"database/sql"
	"time"

	"github.com/go-sql-driver/mysql"
)

// UserDetail is ドメインモデル
// mysql依存のコードになっているため、依存部分切り出しの必要あり
type UserDetail struct {
	UserID                  int            `db:"user_id"`
	BrandID                 int            `db:"brand_id"`
	LabelID                 int            `db:"label_id"`
	ShopID                  int            `db:"shop_id"`
	GroupID                 int            `db:"group_id"`
	UserCode                string         `db:"user_code"`
	Name                    string         `db:"name"`
	NameKana                string         `db:"name_kana"`
	Tel                     string         `db:"tel"`
	Height                  int            `db:"height"`
	Gender                  int            `db:"gender"`
	Profile                 sql.NullString `db:"profile"`
	BirthDate               sql.NullString `db:"birth_date"`
	Birthplace              int            `db:"birthplace"`
	HireDate                sql.NullString `db:"hire_date"`
	EasyPass                string         `db:"easy_pass"`
	Img                     string         `db:"img"`
	BackgroundImg           string         `db:"background_img"`
	IsTeam                  int            `db:"is_team"`
	UserDetailLinkClientApp string         `db:"user_detail_link_client_app"`
	Retirement              int            `db:"retirement"`
	RetirementDate          sql.NullString `db:"retirement_date"`
	IsLinked                int            `db:"is_linked"`
	CreatedAt               time.Time      `db:"created_at"`
	UpdatedAt               time.Time      `db:"updated_at"`
	DeletedAt               mysql.NullTime `db:"deleted_at"`
}

// GetAge is UserDetail.BirthDateより年齢を取得
func (ud *UserDetail) GetAge() (int, error) {
	if !ud.BirthDate.Valid {
		return 0, nil
	}

	const layout = "20060102"
	birthdate, err := time.Parse(layout, ud.BirthDate.String)
	if err != nil {
		return 0, err
	}
	return age(birthdate), nil
}

func age(birthday time.Time) int {
	now := time.Now()
	years := now.Year() - birthday.Year()
	if now.YearDay() < birthday.YearDay() {
		years--
	}
	return years
}
