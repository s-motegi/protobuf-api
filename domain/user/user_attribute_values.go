package user

import (
	"time"

	"github.com/go-sql-driver/mysql"
)

// UserAttributeValues ...
type UserAttributeValues struct {
	ID                  int            `db:"id"`
	BrandID             int            `db:"brand_id"`
	UserAttributeTypeID int            `db:"user_attribute_type_id"`
	Slug                string         `db:"slug"`
	DisplayValue        string         `db:"display_value"`
	CreatedAt           time.Time      `db:"created_at"`
	UpdatedAt           time.Time      `db:"updated_at"`
	DeletedAt           mysql.NullTime `db:"deleted_at"`
}
