package user

import "time"

// UserExternalServiceAttribute ...
type UserExternalServiceAttribute struct {
	ID           int       `db:"id"`
	UserID       int       `db:"user_id"`
	ServiceType  int       `db:"service_type"`
	LinkValue    string    `db:"link_value"`
	DisplayValue string    `db:"display_value"`
	CreatedAt    time.Time `db:"created_at"`
	UpdatedAt    time.Time `db:"updated_at"`
	DeletedAt    time.Time `db:"deleted_at"`
}
