package user

import (
	"database/sql"
	"time"

	"github.com/go-sql-driver/mysql"
)

// User ...
type User struct {
	ID                int            `db:"id"`
	Email             string         `db:"email"`
	Password          string         `db:"password"`
	Permissions       sql.NullString `db:"permissions"`
	Activated         int            `db:"activated"`
	ActivationCode    sql.NullString `db:"activation_code"`
	ActivatetAt       mysql.NullTime `db:"activated_at"`
	LastLogin         mysql.NullTime `db:"last_login"`
	PersistCode       sql.NullString `db:"persist_code"`
	ResetPasswordCode sql.NullString `db:"reset_password_code"`
	FirstName         sql.NullString `db:"first_name"`
	LastName          sql.NullString `db:"last_name"`
	CreatedAt         time.Time      `db:"created_at"`
	UpdatedAt         time.Time      `db:"updated_at"`
	DeletedAt         mysql.NullTime `db:"deleted_at"`
}
