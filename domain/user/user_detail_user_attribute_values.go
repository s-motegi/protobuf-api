package user

import (
	"time"

	"github.com/go-sql-driver/mysql"
)

// UserDetailUserAttributeValues ...
type UserDetailUserAttributeValues struct {
	ID                   int            `db:"id"`
	BrandID              int            `db:"brand_id"`
	UserID               int            `db:"user_id"`
	UserAttributeTypeID  int            `db:"user_attribute_type_id"`
	UserAttributeValueID int            `db:"user_attribute_value_id"`
	CreatedAt            time.Time      `db:"created_at"`
	UpdatedAt            time.Time      `db:"updated_at"`
	DeletedAt            mysql.NullTime `db:"deleted_at"`
}
