package article

import (
	"database/sql"
	"time"
)

// Article ...
type Article struct {
	ID               int          `db:"id"`
	UserID           int          `db:"user_id"`
	BrandID          int          `db:"brand_id"`
	LabelID          int          `db:"label_id"`
	ShopID           int          `db:"shop_id"`
	ArticleTypeID    int          `db:"article_type_id"`
	Title            string       `db:"title"`
	ThumbnailURL     string       `db:"thumbnail_url"`
	Description      string       `db:"description"`
	IsDraft          int          `db:"is_draft"`
	IsAccepted       int          `db:"is_accepted"`
	AcceptedAt       sql.NullTime `db:"accepted_at"`
	PublishedAt      sql.NullTime `db:"published_at"`
	FirstPublishedAt sql.NullTime `db:"first_published_at"`
	LastPublishedAt  sql.NullTime `db:"last_published_at"`
	CreatedAt        time.Time    `db:"created_at"`
	UpdatedAt        time.Time    `db:"updated_at"`
	DeletedAt        sql.NullTime `db:"deleted_at"`
	PV               int          `db:"pv"`
	Purchase         int          `db:"purchase"`
}
