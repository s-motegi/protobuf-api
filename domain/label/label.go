package domain

import (
	"time"

	"github.com/go-sql-driver/mysql"
)

// Label ...
type Label struct {
	ID                          int            `db:"id"`
	BrandID                     int            `db:"brand_id"`
	Code                        string         `db:"code"`
	Name                        string         `db:"name"`
	NameKana                    string         `db:"name_kana"`
	ProductAttributeCodeLabelID int            `db:"product_attribute_code_label_id"`
	CreatedAt                   time.Time      `db:"created_at"`
	UpdatedAt                   time.Time      `db:"updated_at"`
	DeletedAt                   mysql.NullTime `db:"deleted_at"`
}
