package domain

import (
	"database/sql"
	"time"

	"github.com/go-sql-driver/mysql"
)

// Shop ...
type Shop struct {
	ID            int            `db:"id"`
	BrandID       int            `db:"brand_id"`
	ShopCode      string         `db:"shop_code"`
	Name          string         `db:"name"`
	NameKana      string         `db:"name_kana"`
	Zip           string         `db:"zip"`
	Address       string         `db:"address"`
	Staff         string         `db:"staff"`
	Email         string         `db:"email"`
	Tel           string         `db:"tel"`
	MainImageURL  string         `db:"main_image_url"`
	IconImageURL  string         `db:"icon_image_url"`
	Description   sql.NullString `db:"description"`
	Longitude     sql.NullString `db:"longitude"`
	Latitude      sql.NullString `db:"latitude"`
	BusinessHours string         `db:"business_hours"`
	ShopHolidays  string         `db:"shop_holidays"`
	CreatedAt     time.Time      `db:"created_at"`
	UpdatedAt     time.Time      `db:"updated_at"`
	DeletedAt     mysql.NullTime `db:"deleted_at"`
}
