FROM golang:1.14

WORKDIR /go/app

COPY . /go/app

RUN go mod tidy && \
    go get github.com/oxequa/realize
