package main

import (
	"bitbucket.org/s-motegi/protobuf-api/driver"
)

func main() {
	driver.APIServer.Start(":8080")
}
