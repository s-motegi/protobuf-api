package label

import domain "bitbucket.org/s-motegi/protobuf-api/domain/label"

// Repository ...
type Repository interface {
	FindByID(id int) (*domain.Label, error)
}
