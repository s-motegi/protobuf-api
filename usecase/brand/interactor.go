package brand

import (
	domain "bitbucket.org/s-motegi/protobuf-api/domain/brand"
)

// Interactor ...
type Interactor struct {
	Repository
}

// NewInteractor ...
func NewInteractor(r Repository) Interactor {
	return Interactor{r}
}

// FindByID ...
func (interactor *Interactor) FindByID(id int) (*domain.Brand, error) {
	return interactor.Repository.FindByID(id)
}
