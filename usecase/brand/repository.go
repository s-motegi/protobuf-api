package brand

import domain "bitbucket.org/s-motegi/protobuf-api/domain/brand"

// Repository is アダプターレイヤにて実装されるドメインモデル取得用のインターフェース
type Repository interface {
	FindByID(id int) (*domain.Brand, error)
}
