package shop

import (
	domain "bitbucket.org/s-motegi/protobuf-api/domain/shop"
)

// Interactor ...
type Interactor struct {
	Repository
}

// NewInteractor ...
func NewInteractor(r Repository) Interactor {
	return Interactor{r}
}

// FindByID ...
func (interactor *Interactor) FindByID(id int) (*domain.Shop, error) {
	return interactor.Repository.FindByID(id)
}
