package shop

import domain "bitbucket.org/s-motegi/protobuf-api/domain/shop"

// Repository ...
type Repository interface {
	FindByID(id int) (*domain.Shop, error)
}
