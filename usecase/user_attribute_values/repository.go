package usrattval

import domain "bitbucket.org/s-motegi/protobuf-api/domain/user"

// Repository ...
type Repository interface {
	FindByID(id int) (*domain.UserAttributeValues, error)
}
