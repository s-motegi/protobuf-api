package usrattval

import (
	domain "bitbucket.org/s-motegi/protobuf-api/domain/user"
)

// Interactor ...
type Interactor struct {
	Repository
}

// NewInteractor ...
func NewInteractor(r Repository) Interactor {
	return Interactor{r}
}

// FindByID ...
func (interactor *Interactor) FindByID(id int) (*domain.UserAttributeValues, error) {
	return interactor.Repository.FindByID(id)
}
