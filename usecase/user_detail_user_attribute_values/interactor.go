package usrdtlattval

import (
	domain "bitbucket.org/s-motegi/protobuf-api/domain/user"
)

// Interactor ...
type Interactor struct {
	Repository
}

// NewInteractor ...
func NewInteractor(r Repository) Interactor {
	return Interactor{r}
}

// FindByUserID ...
func (interactor *Interactor) FindByUserID(userid int) ([]*domain.UserDetailUserAttributeValues, error) {
	return interactor.Repository.FindByUserID(userid)
}
