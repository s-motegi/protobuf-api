package usrdtlattval

import domain "bitbucket.org/s-motegi/protobuf-api/domain/user"

// Repository ...
type Repository interface {
	FindByUserID(userid int) ([]*domain.UserDetailUserAttributeValues, error)
}
