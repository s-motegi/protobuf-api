package coordinate

// Repository ...
type Repository interface {
	IsPostedByUserID(userid int) (bool, error)
}
