package coordinate

// Interactor ...
type Interactor struct {
	Repository
}

// NewInteractor ...
func NewInteractor(r Repository) Interactor {
	return Interactor{r}
}

// IsPostedByUserID is 指定されたユーザーがコーディネートを投稿しているユーザーかを判定
func (interactor *Interactor) IsPostedByUserID(userid int) (bool, error) {
	return interactor.Repository.IsPostedByUserID(userid)
}
