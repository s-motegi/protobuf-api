package usrextatt

import (
	domain "bitbucket.org/s-motegi/protobuf-api/domain/user"
)

// Interactor ...
type Interactor struct {
	Repository
}

// NewInteractor ...
func NewInteractor(r Repository) Interactor {
	return Interactor{r}
}

// FindByUserID ...
func (interactor *Interactor) FindByUserID(id int) (*domain.UserExternalServiceAttribute, error) {
	return interactor.Repository.FindByUserID(id)
}
