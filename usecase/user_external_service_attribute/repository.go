package usrextatt

import domain "bitbucket.org/s-motegi/protobuf-api/domain/user"

// Repository ...
type Repository interface {
	FindByUserID(id int) (*domain.UserExternalServiceAttribute, error)
}
