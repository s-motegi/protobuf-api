package article

// Interactor ...
type Interactor struct {
	Repository
}

// NewInteractor ...
func NewInteractor(r Repository) Interactor {
	return Interactor{r}
}

// IsPostedByUserID ...
func (interactor *Interactor) IsPostedByUserID(userid int) (bool, error) {
	return interactor.Repository.IsPostedByUserID(userid)
}
