package article

// Repository ...
type Repository interface {
	IsPostedByUserID(userid int) (bool, error)
}
