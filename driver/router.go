package driver

import (
	"context"
	"net/http"

	"github.com/golang/protobuf/proto"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	pb "bitbucket.org/s-motegi/protobuf-api/adapter/proto"
	"bitbucket.org/s-motegi/protobuf-api/adapter/userdetail"
	"bitbucket.org/s-motegi/protobuf-api/driver/mysql"
	"go.uber.org/zap"
)

// APIServer ...
var APIServer struct {
	*echo.Echo
	*zap.Logger
}

func init() {
	e := echo.New()
	logger, _ := zap.NewDevelopment()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	conv := pb.NewStaffDetailServiceHTTPConverter(userdetail.NewController(mysql.NewSQLHandler()))
	// conv2 := pb.NewStaffListServiceHTTPConverter(user.NewController(mysql.NewSQLHandler()))

	method, path, handlerFunc := conv.StaffDetailHTTPRule(callBack)
	// method2, path2, handlerFunc2 := conv2.StaffListHTTPRule(callBack)

	e.Add(method, path, echo.WrapHandler(handlerFunc))
	// e.Add(method2, path2, echo.WrapHandler(handlerFunc2))

	APIServer.Echo = e
	APIServer.Logger = logger
}

func callBack(ctx context.Context, w http.ResponseWriter, r *http.Request, arg, ret proto.Message, err error) {
	if err != nil {
		APIServer.Logger.Error(err.Error())
	}
}
