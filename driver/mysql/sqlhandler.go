package mysql

import (
	"database/sql"

	"bitbucket.org/s-motegi/protobuf-api/adapter/database"

	// package mysql内のdriver.go > init()にて、
	// sql.Registerを行なっているためblank import
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

// SQLHandler ...
type SQLHandler struct {
	Conn *sqlx.DB
}

// NewSQLHandler ...
func NewSQLHandler() *SQLHandler {
	conn, err := sqlx.Open("mysql", "root:@tcp(protobuf-api_mysql_1:3306)/oxford?parseTime=true&loc=Asia%2FTokyo")
	if err != nil {
		panic(err.Error)
	}
	sqlHandler := new(SQLHandler)
	sqlHandler.Conn = conn
	return sqlHandler
}

// Execute ...
func (handler *SQLHandler) Execute(stmt string, args ...interface{}) (database.Result, error) {
	return nil, nil
}

// QueryOne ...
func (handler *SQLHandler) QueryOne(stmt string, args ...interface{}) (database.Row, error) {
	row := handler.Conn.QueryRowx(stmt, args...)
	return row, row.Err()
}

// Query ...
func (handler *SQLHandler) Query(stmt string, args ...interface{}) (database.Rows, error) {
	return handler.Conn.Queryx(stmt, args...)
}

// Get ...
func (handler *SQLHandler) Get(dest interface{}, stmt string, args ...interface{}) error {
	err := handler.Conn.Get(dest, stmt, args...)
	if err == sql.ErrNoRows {
		return database.ErrNoData
	}
	return err
}
