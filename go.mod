module bitbucket.org/s-motegi/protobuf-api

go 1.14

require (
	github.com/envoyproxy/protoc-gen-validate v0.1.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.4.2
	github.com/jmoiron/sqlx v1.2.0
	github.com/labstack/echo/v4 v4.1.16
	github.com/stretchr/testify v1.5.1 // indirect
	go.uber.org/zap v1.15.0
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37 // indirect
	golang.org/x/net v0.0.0-20200513185701-a91f0712d120 // indirect
	golang.org/x/sys v0.0.0-20200515095857-1151b9dac4a9 // indirect
	google.golang.org/genproto v0.0.0-20200515170657-fc4c6c6a6587
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.23.0
	gopkg.in/yaml.v2 v2.3.0 // indirect

)

replace gopkg.in/urfave/cli.v2 => github.com/urfave/cli/v2 v2.2.0
