package article

import (
	"time"

	"bitbucket.org/s-motegi/protobuf-api/adapter/database"
	domain "bitbucket.org/s-motegi/protobuf-api/domain/article"
	"bitbucket.org/s-motegi/protobuf-api/usecase/article"
)

// Repository ...
type Repository struct {
	database.SQLHandler
}

// NewRepository ...
func NewRepository(h database.SQLHandler) article.Repository {
	return &Repository{h}
}

// IsPostedByUserID ...
func (repo *Repository) IsPostedByUserID(userid int) (bool, error) {
	article := &domain.Article{}

	err := repo.SQLHandler.Get(
		article,
		"SELECT * FROM articles WHERE user_id = ? AND is_draft = ? AND is_accepted = ? AND last_published_at <= ? AND deleted_at IS NULL",
		userid,
		0,
		1,
		time.Now())
	if err != nil {
		return false, err
	}
	return true, nil
}
