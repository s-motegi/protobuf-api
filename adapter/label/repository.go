package label

import (
	"bitbucket.org/s-motegi/protobuf-api/adapter/database"
	domain "bitbucket.org/s-motegi/protobuf-api/domain/label"
	"bitbucket.org/s-motegi/protobuf-api/usecase/label"
)

// Repository ...
type Repository struct {
	database.SQLHandler
}

// NewRepository ...
func NewRepository(h database.SQLHandler) label.Repository {
	return &Repository{h}
}

// FindByID ...
func (repo *Repository) FindByID(id int) (*domain.Label, error) {
	label := &domain.Label{}

	if err := repo.SQLHandler.Get(label, "SELECT * FROM labels WHERE id = ?", id); err != nil {
		return nil, err
	}
	return label, nil
}
