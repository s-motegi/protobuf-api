package usrdtlattval

import (
	"bitbucket.org/s-motegi/protobuf-api/adapter/database"
	domain "bitbucket.org/s-motegi/protobuf-api/domain/user"
	"bitbucket.org/s-motegi/protobuf-api/usecase/user_detail_user_attribute_values"
)

// Repository ...
type Repository struct {
	database.SQLHandler
}

// NewRepository ...
func NewRepository(h database.SQLHandler) usrdtlattval.Repository {
	return &Repository{h}
}

// FindByUserID ...
func (repo *Repository) FindByUserID(id int) ([]*domain.UserDetailUserAttributeValues, error) {
	rows, err := repo.SQLHandler.Query("SELECT * FROM user_detail_user_attribute_values WHERE user_id = ?", id)
	if err != nil {
		return nil, err
	}

	var vals []*domain.UserDetailUserAttributeValues
	val := &domain.UserDetailUserAttributeValues{}

	for rows.Next() {
		err := rows.StructScan(val)
		if err != nil {
			return nil, err
		}
		vals = append(vals, val)
	}
	return vals, nil
}
