package database

import "errors"

// ErrNoData is adapters層のcontrollerでのDB呼び出しをMySQLに依存させないため
var ErrNoData = errors.New("NoData")
