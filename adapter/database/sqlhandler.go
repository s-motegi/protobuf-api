package database

// SQLHandler ...
type SQLHandler interface {
	Execute(string, ...interface{}) (Result, error)
	QueryOne(string, ...interface{}) (Row, error)
	Query(query string, args ...interface{}) (Rows, error)
	Get(dest interface{}, query string, args ...interface{}) error
}

// Result ...
type Result interface {
	LastInsertId() (int64, error)
	RowsAffected() (int64, error)
}

// Rows ...
type Rows interface {
	Next() bool
	StructScan(dest interface{}) error
}

// Row is 一レコードに対する操作メソッド群
type Row interface {
	Scan(...interface{}) error
	// Close() error
}
