package usrextatt

import (
	"bitbucket.org/s-motegi/protobuf-api/adapter/database"
	domain "bitbucket.org/s-motegi/protobuf-api/domain/user"
	"bitbucket.org/s-motegi/protobuf-api/usecase/user_external_service_attribute"
)

// Repository ...
type Repository struct {
	database.SQLHandler
}

// NewRepository ...
func NewRepository(h database.SQLHandler) usrextatt.Repository {
	return &Repository{h}
}

// FindByUserID ...
func (repo *Repository) FindByUserID(id int) (*domain.UserExternalServiceAttribute, error) {
	att := &domain.UserExternalServiceAttribute{}

	err := repo.SQLHandler.Get(att, "SELECT * FROM user_external_service_attributes WHERE user_id = ?", id)
	return att, err
}
