package coordinate

import (
	"time"

	"bitbucket.org/s-motegi/protobuf-api/adapter/database"
	domain "bitbucket.org/s-motegi/protobuf-api/domain/coordinate"
	"bitbucket.org/s-motegi/protobuf-api/usecase/coordinate"
)

// Repository ...
type Repository struct {
	database.SQLHandler
}

// NewRepository ...
func NewRepository(h database.SQLHandler) coordinate.Repository {
	return &Repository{h}
}

// IsPostedByUserID ...
func (repo *Repository) IsPostedByUserID(userid int) (bool, error) {
	coordinate := &domain.Coordinate{}

	err := repo.SQLHandler.Get(
		coordinate,
		"SELECT * FROM coordinates WHERE user_id = ? AND accept = ? AND accept_at <= ? AND deleted_at IS NULL ",
		userid,
		1,
		time.Now())

	if err != nil {
		return false, err
	}
	return true, nil
}
