package user

import (
	"context"

	"bitbucket.org/s-motegi/protobuf-api/adapter/database"
	"bitbucket.org/s-motegi/protobuf-api/usecase/user"
)

// Controller ...
type Controller struct {
	UserInteractor user.Interactor
}

// NewController ...
func NewController(sqlHandler database.SQLHandler) *Controller {
	return &Controller{
		UserInteractor: user.NewInteractor(NewRepository(sqlHandler)),
	}
}

// StaffList ...
func (controller *Controller) StaffList(ctx context.Context, input *pb.StaffListRequest) (*pb.StaffListResponse, error) {
	return nil, nil
}
