package shop

import (
	"bitbucket.org/s-motegi/protobuf-api/adapter/database"
	domain "bitbucket.org/s-motegi/protobuf-api/domain/shop"
	"bitbucket.org/s-motegi/protobuf-api/usecase/shop"
)

// Repository ...
type Repository struct {
	database.SQLHandler
}

// NewRepository ...
func NewRepository(h database.SQLHandler) shop.Repository {
	return &Repository{h}
}

// FindByID ...
func (repo *Repository) FindByID(id int) (*domain.Shop, error) {
	shop := &domain.Shop{}

	if err := repo.SQLHandler.Get(shop, "SELECT * FROM shops WHERE id = ?", id); err != nil {
		return nil, err
	}
	return shop, nil
}
