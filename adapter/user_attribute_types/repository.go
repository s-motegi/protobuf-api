package usratttype

import (
	"bitbucket.org/s-motegi/protobuf-api/adapter/database"
	domain "bitbucket.org/s-motegi/protobuf-api/domain/user"
	"bitbucket.org/s-motegi/protobuf-api/usecase/user_attribute_types"
)

// Repository ...
type Repository struct {
	database.SQLHandler
}

// NewRepository ...
func NewRepository(h database.SQLHandler) usratttype.Repository {
	return &Repository{h}
}

// FindByID ...
func (repo *Repository) FindByID(id int) (*domain.UserAttributeTypes, error) {
	types := &domain.UserAttributeTypes{}
	if err := repo.SQLHandler.Get(types, "SELECT * FROM user_attribute_types WHERE id = ?", id); err != nil {
		return nil, err
	}

	return types, nil
}
