package brand

import (
	"bitbucket.org/s-motegi/protobuf-api/adapter/database"
	domain "bitbucket.org/s-motegi/protobuf-api/domain/brand"
	"bitbucket.org/s-motegi/protobuf-api/usecase/brand"
)

// Repository is ユースケースレイヤに定義したRepositoryインターフェースの実装
type Repository struct {
	database.SQLHandler
}

// NewRepository ...
func NewRepository(h database.SQLHandler) brand.Repository {
	return &Repository{h}
}

// FindByID ...
func (repo *Repository) FindByID(id int) (*domain.Brand, error) {
	brand := &domain.Brand{}

	err := repo.SQLHandler.Get(brand, "SELECT * FROM brands WHERE id = ?", id)
	return brand, err
}
