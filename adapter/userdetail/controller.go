package userdetail

import (
	"context"
	"strconv"
	"strings"

	articlerepo "bitbucket.org/s-motegi/protobuf-api/adapter/article"
	brandrepo "bitbucket.org/s-motegi/protobuf-api/adapter/brand"
	coordinaterepo "bitbucket.org/s-motegi/protobuf-api/adapter/coordinate"
	"bitbucket.org/s-motegi/protobuf-api/adapter/database"
	labelrepo "bitbucket.org/s-motegi/protobuf-api/adapter/label"
	pb "bitbucket.org/s-motegi/protobuf-api/adapter/proto"
	shoprepo "bitbucket.org/s-motegi/protobuf-api/adapter/shop"
	usratttyperepo "bitbucket.org/s-motegi/protobuf-api/adapter/user_attribute_types"
	usrattvalrepo "bitbucket.org/s-motegi/protobuf-api/adapter/user_attribute_values"
	usrdtlattvalrepo "bitbucket.org/s-motegi/protobuf-api/adapter/user_detail_user_attribute_values"
	usrextattrepo "bitbucket.org/s-motegi/protobuf-api/adapter/user_external_service_attribute"
	"bitbucket.org/s-motegi/protobuf-api/usecase/article"
	"bitbucket.org/s-motegi/protobuf-api/usecase/brand"
	"bitbucket.org/s-motegi/protobuf-api/usecase/coordinate"
	"bitbucket.org/s-motegi/protobuf-api/usecase/label"
	"bitbucket.org/s-motegi/protobuf-api/usecase/shop"
	usratttype "bitbucket.org/s-motegi/protobuf-api/usecase/user_attribute_types"
	usrattval "bitbucket.org/s-motegi/protobuf-api/usecase/user_attribute_values"
	usrdtlattval "bitbucket.org/s-motegi/protobuf-api/usecase/user_detail_user_attribute_values"
	usrextatt "bitbucket.org/s-motegi/protobuf-api/usecase/user_external_service_attribute"
	"bitbucket.org/s-motegi/protobuf-api/usecase/userdetail"
)

// Controller ...
type Controller struct {
	UserDetailInteractor   userdetail.Interactor
	BrandInteractor        brand.Interactor
	ShopInteractor         shop.Interactor
	LabelInteractor        label.Interactor
	UsrExtAttInteractor    usrextatt.Interactor
	CoordinateInteractor   coordinate.Interactor
	ArticleInteractor      article.Interactor
	UsrDtlAttValInteractor usrdtlattval.Interactor
	UsrAttTypesInteractor  usratttype.Interactor
	UsrAttValuesInteractor usrattval.Interactor
}

// NewController ...
func NewController(sqlHandler database.SQLHandler) *Controller {
	return &Controller{
		UserDetailInteractor:   userdetail.NewInteractor(NewRepository(sqlHandler)),
		BrandInteractor:        brand.NewInteractor(brandrepo.NewRepository(sqlHandler)),
		ShopInteractor:         shop.NewInteractor(shoprepo.NewRepository(sqlHandler)),
		LabelInteractor:        label.NewInteractor(labelrepo.NewRepository(sqlHandler)),
		UsrExtAttInteractor:    usrextatt.NewInteractor(usrextattrepo.NewRepository(sqlHandler)),
		CoordinateInteractor:   coordinate.NewInteractor(coordinaterepo.NewRepository(sqlHandler)),
		ArticleInteractor:      article.NewInteractor(articlerepo.NewRepository(sqlHandler)),
		UsrDtlAttValInteractor: usrdtlattval.NewInteractor(usrdtlattvalrepo.NewRepository(sqlHandler)),
		UsrAttTypesInteractor:  usratttype.NewInteractor(usratttyperepo.NewRepository(sqlHandler)),
		UsrAttValuesInteractor: usrattval.NewInteractor(usrattvalrepo.NewRepository(sqlHandler)),
	}
}

// StaffDetail ...
func (controller *Controller) StaffDetail(ctx context.Context, input *pb.StaffDetailRequest) (*pb.StaffDetailResponse, error) {
	if err := input.Validate(); err != nil {
		return nil, err
	}

	userdetail, err := controller.UserDetailInteractor.FindByID(int(input.UserId))
	if err == database.ErrNoData {
		return &pb.StaffDetailResponse{
			Code:   "1",
			Total:  0,
			Params: input,
		}, nil
	} else if err != nil {
		return nil, err
	}

	brand, err := controller.BrandInteractor.FindByID(userdetail.BrandID)
	if err != nil {
		return nil, err
	}

	if !brand.ValidMerchantID(input.MerchantId) {
		return &pb.StaffDetailResponse{
			Code: "-1",
			Message: []string{
				"The selected merchant id is invalid.",
			},
		}, nil
	}

	shop, err := controller.ShopInteractor.FindByID(userdetail.ShopID)
	if err != nil {
		return nil, err
	}

	label, err := controller.LabelInteractor.FindByID(userdetail.LabelID)
	if err != nil {
		return nil, err
	}

	usrExtAtt, err := controller.UsrExtAttInteractor.FindByUserID(int(input.UserId))
	if err != nil {
		return nil, err
	}

	age, err := userdetail.GetAge()
	if err != nil {
		return nil, err
	}

	isPostedCoordinate, err := controller.CoordinateInteractor.IsPostedByUserID(int(input.UserId))
	if err != nil && err != database.ErrNoData {
		return nil, err
	}

	isPostedArticle, err := controller.ArticleInteractor.IsPostedByUserID(int(input.UserId))
	if err != nil && err != database.ErrNoData {
		return nil, err
	}

	usrDtlAttVals, err := controller.UsrDtlAttValInteractor.FindByUserID(int(input.UserId))
	if err != nil {
		return nil, err
	}

	userAttributes := []*pb.StaffDetailResponse_Item_UserAttributes{}

	for _, val := range usrDtlAttVals {
		t, err := controller.UsrAttTypesInteractor.FindByID(val.UserAttributeTypeID)
		if err != nil {
			return nil, err
		}
		v, err := controller.UsrAttValuesInteractor.FindByID(val.UserAttributeValueID)
		if err != nil {
			return nil, err
		}
		userAttributes = append(userAttributes, &pb.StaffDetailResponse_Item_UserAttributes{
			UserAttributeType: &pb.StaffDetailResponse_Item_UserAttributes_UserAttributeType{
				Slug:         t.Slug,
				DisplayValue: t.DisplayValue,
			},
			UserAttributeValue: &pb.StaffDetailResponse_Item_UserAttributes_UserAttributeValue{
				Slug:         v.Slug,
				DisplayValue: v.DisplayValue,
			},
		})
	}

	return &pb.StaffDetailResponse{
		Code:  "1",
		Total: 1,
		Item: []*pb.StaffDetailResponse_Item{
			&pb.StaffDetailResponse_Item{
				UserId:                  int64(userdetail.UserID),
				BrandId:                 int64(userdetail.BrandID),
				Name:                    userdetail.Name,
				NameKana:                userdetail.NameKana,
				Height:                  int64(userdetail.Height),
				Gender:                  int32(userdetail.Gender),
				Age:                     int32(age),
				Profile:                 userdetail.Profile.String,
				Img:                     userdetail.Img,
				ResizedImages:           getResizedImages(userdetail.Img),
				BackgroundImageUrls:     getBackgroundImageURLs(userdetail.BackgroundImg),
				LabelId:                 int64(userdetail.LabelID),
				LabelName:               label.Name,
				LabelCode:               label.Code,
				ShopId:                  int64(userdetail.ShopID),
				ShopName:                shop.Name,
				ShopCode:                shop.ShopCode,
				IsTeam:                  strconv.Itoa(userdetail.IsTeam),
				UserDetailLinkClientApp: userdetail.UserDetailLinkClientApp,
				LinkValue:               usrExtAtt.LinkValue,
				DisplayValue:            usrExtAtt.DisplayValue,
				IsPostedContents: &pb.StaffDetailResponse_Item_IsPostedContents{
					Coordinate: isPostedCoordinate,
					Article:    isPostedArticle,
				},
				IsPostedContentsAlhd: makeIsPostedContentsAlhd(isPostedCoordinate, isPostedArticle),
				UserAttributes:       userAttributes,
			},
		},
		Params: input,
	}, nil
}

func getResizedImages(img string) []*pb.StaffDetailResponse_Item_ResizedImages {
	pos := strings.LastIndex(img, ".")
	if pos == -1 {
		return []*pb.StaffDetailResponse_Item_ResizedImages{}
	}

	return func(sizes []string) (results []*pb.StaffDetailResponse_Item_ResizedImages) {
		for _, size := range sizes {
			results = append(
				results,
				&pb.StaffDetailResponse_Item_ResizedImages{
					Size: size,
					Url:  img[:pos] + "_" + size + img[pos:],
				},
			)
		}
		return
	}([]string{"s", "m", "l"})
}

func getBackgroundImageURLs(img string) []*pb.StaffDetailResponse_Item_BackgroundImageURLs {
	pos := strings.LastIndex(img, ".")
	if pos == -1 {
		return []*pb.StaffDetailResponse_Item_BackgroundImageURLs{}
	}

	resizer := func(img string) func(size string) string {
		return func(size string) string {
			return img[:pos] + "_" + size + img[pos:]
		}
	}(img)

	return []*pb.StaffDetailResponse_Item_BackgroundImageURLs{
		&pb.StaffDetailResponse_Item_BackgroundImageURLs{
			Org: img,
			S:   resizer("s"),
			M:   resizer("m"),
			L:   resizer("l"),
		},
	}
}

func makeIsPostedContentsAlhd(coordinate, article bool) []*pb.StaffDetailResponse_Item_IsPostedContentsAlhd {

	convert := func(b bool) string {
		if b {
			return "1"
		}
		return ""
	}
	return []*pb.StaffDetailResponse_Item_IsPostedContentsAlhd{
		&pb.StaffDetailResponse_Item_IsPostedContentsAlhd{
			Coordinate: convert(coordinate),
			Article:    convert(article),
		},
	}
}
