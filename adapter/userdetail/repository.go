package userdetail

import (
	"bitbucket.org/s-motegi/protobuf-api/adapter/database"
	domain "bitbucket.org/s-motegi/protobuf-api/domain/user"
	"bitbucket.org/s-motegi/protobuf-api/usecase/userdetail"
)

// Repository ...
type Repository struct {
	database.SQLHandler
}

// NewRepository ...
func NewRepository(h database.SQLHandler) userdetail.Repository {
	return &Repository{h}
}

// FindByID ...
func (repo *Repository) FindByID(id int) (*domain.UserDetail, error) {
	ud := &domain.UserDetail{}

	if err := repo.SQLHandler.Get(ud, "SELECT * FROM user_details WHERE user_id = ?", id); err != nil {
		return nil, err
	}
	return ud, nil
}
