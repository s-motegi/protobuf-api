// Code generated by protoc-gen-gohttp. DO NOT EDIT.
// source: adapter/proto/staff_list.proto

package proto

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// StaffListServiceHTTPConverter has a function to convert StaffListServiceServer interface to http.HandlerFunc.
type StaffListServiceHTTPConverter struct {
	srv StaffListServiceServer
}

// NewStaffListServiceHTTPConverter returns StaffListServiceHTTPConverter.
func NewStaffListServiceHTTPConverter(srv StaffListServiceServer) *StaffListServiceHTTPConverter {
	return &StaffListServiceHTTPConverter{
		srv: srv,
	}
}

// StaffList returns StaffListServiceServer interface's StaffList converted to http.HandlerFunc.
func (h *StaffListServiceHTTPConverter) StaffList(cb func(ctx context.Context, w http.ResponseWriter, r *http.Request, arg, ret proto.Message, err error), interceptors ...grpc.UnaryServerInterceptor) http.HandlerFunc {
	if cb == nil {
		cb = func(ctx context.Context, w http.ResponseWriter, r *http.Request, arg, ret proto.Message, err error) {
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				p := status.New(codes.Unknown, err.Error()).Proto()
				switch r.Header.Get("Content-Type") {
				case "application/protobuf", "application/x-protobuf":
					buf, err := proto.Marshal(p)
					if err != nil {
						return
					}
					if _, err := io.Copy(w, bytes.NewBuffer(buf)); err != nil {
						return
					}
				case "application/json":
					if err := json.NewEncoder(w).Encode(p); err != nil {
						return
					}
				default:
				}
			}
		}
	}
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		arg := &StaffListRequest{}
		contentType := r.Header.Get("Content-Type")
		if r.Method != http.MethodGet {
			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				cb(ctx, w, r, nil, nil, err)
				return
			}

			switch contentType {
			case "application/protobuf", "application/x-protobuf":
				if err := proto.Unmarshal(body, arg); err != nil {
					cb(ctx, w, r, nil, nil, err)
					return
				}
			case "application/json":
				if err := jsonpb.Unmarshal(bytes.NewBuffer(body), arg); err != nil {
					cb(ctx, w, r, nil, nil, err)
					return
				}
			default:
				w.WriteHeader(http.StatusUnsupportedMediaType)
				_, err := fmt.Fprintf(w, "Unsupported Content-Type: %s", contentType)
				cb(ctx, w, r, nil, nil, err)
				return
			}
		}

		n := len(interceptors)
		chained := func(ctx context.Context, arg interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
			chainer := func(currentInter grpc.UnaryServerInterceptor, currentHandler grpc.UnaryHandler) grpc.UnaryHandler {
				return func(currentCtx context.Context, currentReq interface{}) (interface{}, error) {
					return currentInter(currentCtx, currentReq, info, currentHandler)
				}
			}

			chainedHandler := handler
			for i := n - 1; i >= 0; i-- {
				chainedHandler = chainer(interceptors[i], chainedHandler)
			}
			return chainedHandler(ctx, arg)
		}

		info := &grpc.UnaryServerInfo{
			Server:     h.srv,
			FullMethod: "/proto.StaffListService/StaffList",
		}

		handler := func(c context.Context, req interface{}) (interface{}, error) {
			return h.srv.StaffList(c, req.(*StaffListRequest))
		}

		iret, err := chained(ctx, arg, info, handler)
		if err != nil {
			cb(ctx, w, r, arg, nil, err)
			return
		}

		ret, ok := iret.(*StaffListResponse)
		if !ok {
			cb(ctx, w, r, arg, nil, fmt.Errorf("/proto.StaffListService/StaffList: interceptors have not return StaffListResponse"))
			return
		}

		accepts := strings.Split(r.Header.Get("Accept"), ",")
		accept := accepts[0]
		if accept == "*/*" || accept == "" {
			if contentType != "" {
				accept = contentType
			} else {
				accept = "application/json"
			}
		}

		w.Header().Set("Content-Type", accept)

		switch accept {
		case "application/protobuf", "application/x-protobuf":
			buf, err := proto.Marshal(ret)
			if err != nil {
				cb(ctx, w, r, arg, ret, err)
				return
			}
			if _, err := io.Copy(w, bytes.NewBuffer(buf)); err != nil {
				cb(ctx, w, r, arg, ret, err)
				return
			}
		case "application/json":
			m := jsonpb.Marshaler{
				EnumsAsInts:  true,
				EmitDefaults: true,
			}
			if err := m.Marshal(w, ret); err != nil {
				cb(ctx, w, r, arg, ret, err)
				return
			}
		default:
			w.WriteHeader(http.StatusUnsupportedMediaType)
			_, err := fmt.Fprintf(w, "Unsupported Accept: %s", accept)
			cb(ctx, w, r, arg, ret, err)
			return
		}
		cb(ctx, w, r, arg, ret, nil)
	})
}

// StaffListWithName returns Service name, Method name and StaffListServiceServer interface's StaffList converted to http.HandlerFunc.
func (h *StaffListServiceHTTPConverter) StaffListWithName(cb func(ctx context.Context, w http.ResponseWriter, r *http.Request, arg, ret proto.Message, err error), interceptors ...grpc.UnaryServerInterceptor) (string, string, http.HandlerFunc) {
	return "StaffListService", "StaffList", h.StaffList(cb, interceptors...)
}

func (h *StaffListServiceHTTPConverter) StaffListHTTPRule(cb func(ctx context.Context, w http.ResponseWriter, r *http.Request, arg, ret proto.Message, err error), interceptors ...grpc.UnaryServerInterceptor) (string, string, http.HandlerFunc) {
	if cb == nil {
		cb = func(ctx context.Context, w http.ResponseWriter, r *http.Request, arg, ret proto.Message, err error) {
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				p := status.New(codes.Unknown, err.Error()).Proto()
				switch r.Header.Get("Content-Type") {
				case "application/protobuf", "application/x-protobuf":
					buf, err := proto.Marshal(p)
					if err != nil {
						return
					}
					if _, err := io.Copy(w, bytes.NewBuffer(buf)); err != nil {
						return
					}
				case "application/json":
					if err := json.NewEncoder(w).Encode(p); err != nil {
						return
					}
				default:
				}
			}
		}
	}
	return http.MethodGet, "/v1/staff/list", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		arg := &StaffListRequest{}
		contentType := r.Header.Get("Content-Type")
		if r.Method == http.MethodGet {
			if v := r.URL.Query().Get("merchant_id"); v != "" {
				arg.MerchantId = v
			}
			if v := r.URL.Query().Get("count"); v != "" {
				i64, err := strconv.ParseInt(v, 10, 64)
				if err != nil {
					cb(ctx, w, r, nil, nil, err)
					return
				}
				arg.Count = i64
			}
			if v := r.URL.Query().Get("offset"); v != "" {
				i64, err := strconv.ParseInt(v, 10, 64)
				if err != nil {
					cb(ctx, w, r, nil, nil, err)
					return
				}
				arg.Offset = i64
			}
			if v := r.URL.Query().Get("sort"); v != "" {
				arg.Sort = v
			}
			if v := r.URL.Query().Get("label_code"); v != "" {
				arg.LabelCode = v
			}
			if v := r.URL.Query().Get("label_id"); v != "" {
				i64, err := strconv.ParseInt(v, 10, 64)
				if err != nil {
					cb(ctx, w, r, nil, nil, err)
					return
				}
				arg.LabelId = i64
			}
			if v := r.URL.Query().Get("shop_code"); v != "" {
				arg.ShopCode = v
			}
			if v := r.URL.Query().Get("shop_id"); v != "" {
				i64, err := strconv.ParseInt(v, 10, 64)
				if err != nil {
					cb(ctx, w, r, nil, nil, err)
					return
				}
				arg.ShopId = i64
			}
			if v := r.URL.Query().Get("gender"); v != "" {
				arg.Gender = v
			}
			if v := r.URL.Query().Get("from_height"); v != "" {
				i64, err := strconv.ParseInt(v, 10, 64)
				if err != nil {
					cb(ctx, w, r, nil, nil, err)
					return
				}
				arg.FromHeight = i64
			}
			if v := r.URL.Query().Get("to_height"); v != "" {
				i64, err := strconv.ParseInt(v, 10, 64)
				if err != nil {
					cb(ctx, w, r, nil, nil, err)
					return
				}
				arg.ToHeight = i64
			}
			if v := r.URL.Query().Get("user_skin_type"); v != "" {
				arg.UserSkinType = v
			}
			if v := r.URL.Query().Get("user_skin_color"); v != "" {
				arg.UserSkinColor = v
			}
			if v := r.URL.Query().Get("user_eylid"); v != "" {
				arg.UserEylid = v
			}
		}

		n := len(interceptors)
		chained := func(ctx context.Context, arg interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
			chainer := func(currentInter grpc.UnaryServerInterceptor, currentHandler grpc.UnaryHandler) grpc.UnaryHandler {
				return func(currentCtx context.Context, currentReq interface{}) (interface{}, error) {
					return currentInter(currentCtx, currentReq, info, currentHandler)
				}
			}

			chainedHandler := handler
			for i := n - 1; i >= 0; i-- {
				chainedHandler = chainer(interceptors[i], chainedHandler)
			}
			return chainedHandler(ctx, arg)
		}

		info := &grpc.UnaryServerInfo{
			Server:     h.srv,
			FullMethod: "/proto.StaffListService/StaffList",
		}

		handler := func(c context.Context, req interface{}) (interface{}, error) {
			return h.srv.StaffList(c, req.(*StaffListRequest))
		}

		iret, err := chained(ctx, arg, info, handler)
		if err != nil {
			cb(ctx, w, r, arg, nil, err)
			return
		}

		ret, ok := iret.(*StaffListResponse)
		if !ok {
			cb(ctx, w, r, arg, nil, fmt.Errorf("/proto.StaffListService/StaffList: interceptors have not return StaffListResponse"))
			return
		}

		accepts := strings.Split(r.Header.Get("Accept"), ",")
		accept := accepts[0]
		if accept == "*/*" || accept == "" {
			if contentType != "" {
				accept = contentType
			} else {
				accept = "application/json"
			}
		}

		w.Header().Set("Content-Type", accept)

		switch accept {
		case "application/protobuf", "application/x-protobuf":
			buf, err := proto.Marshal(ret)
			if err != nil {
				cb(ctx, w, r, arg, ret, err)
				return
			}
			if _, err := io.Copy(w, bytes.NewBuffer(buf)); err != nil {
				cb(ctx, w, r, arg, ret, err)
				return
			}
		case "application/json":
			m := jsonpb.Marshaler{
				EnumsAsInts:  true,
				EmitDefaults: true,
			}
			if err := m.Marshal(w, ret); err != nil {
				cb(ctx, w, r, arg, ret, err)
				return
			}
		default:
			w.WriteHeader(http.StatusUnsupportedMediaType)
			_, err := fmt.Fprintf(w, "Unsupported Accept: %s", accept)
			cb(ctx, w, r, arg, ret, err)
			return
		}
		cb(ctx, w, r, arg, ret, nil)
	})
}
