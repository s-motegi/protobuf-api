// Code generated by protoc-gen-validate. DO NOT EDIT.
// source: adapter/proto/articles_detail.proto

package proto

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"net/mail"
	"net/url"
	"regexp"
	"strings"
	"time"
	"unicode/utf8"

	"github.com/golang/protobuf/ptypes"
)

// ensure the imports are used
var (
	_ = bytes.MinRead
	_ = errors.New("")
	_ = fmt.Print
	_ = utf8.UTFMax
	_ = (*regexp.Regexp)(nil)
	_ = (*strings.Reader)(nil)
	_ = net.IPv4len
	_ = time.Duration(0)
	_ = (*url.URL)(nil)
	_ = (*mail.Address)(nil)
	_ = ptypes.DynamicAny{}
)

// define the regex for a UUID once up-front
var _articles_detail_uuidPattern = regexp.MustCompile("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$")

// Validate checks the field values on ArticlesDetailRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, an error is returned.
func (m *ArticlesDetailRequest) Validate() error {
	if m == nil {
		return nil
	}

	if utf8.RuneCountInString(m.GetMerchantId()) != 32 {
		return ArticlesDetailRequestValidationError{
			field:  "MerchantId",
			reason: "value length must be 32 runes",
		}

	}

	// no validation rules for ArticleId

	return nil
}

// ArticlesDetailRequestValidationError is the validation error returned by
// ArticlesDetailRequest.Validate if the designated constraints aren't met.
type ArticlesDetailRequestValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ArticlesDetailRequestValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ArticlesDetailRequestValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ArticlesDetailRequestValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ArticlesDetailRequestValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ArticlesDetailRequestValidationError) ErrorName() string {
	return "ArticlesDetailRequestValidationError"
}

// Error satisfies the builtin error interface
func (e ArticlesDetailRequestValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sArticlesDetailRequest.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ArticlesDetailRequestValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ArticlesDetailRequestValidationError{}

// Validate checks the field values on ArticlesDetailResponse with the rules
// defined in the proto definition for this message. If any rules are
// violated, an error is returned.
func (m *ArticlesDetailResponse) Validate() error {
	if m == nil {
		return nil
	}

	// no validation rules for Code

	for idx, item := range m.GetItem() {
		_, _ = idx, item

		if v, ok := interface{}(item).(interface{ Validate() error }); ok {
			if err := v.Validate(); err != nil {
				return ArticlesDetailResponseValidationError{
					field:  fmt.Sprintf("Item[%v]", idx),
					reason: "embedded message failed validation",
					cause:  err,
				}
			}
		}

	}

	if v, ok := interface{}(m.GetParams()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return ArticlesDetailResponseValidationError{
				field:  "Params",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	return nil
}

// ArticlesDetailResponseValidationError is the validation error returned by
// ArticlesDetailResponse.Validate if the designated constraints aren't met.
type ArticlesDetailResponseValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ArticlesDetailResponseValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ArticlesDetailResponseValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ArticlesDetailResponseValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ArticlesDetailResponseValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ArticlesDetailResponseValidationError) ErrorName() string {
	return "ArticlesDetailResponseValidationError"
}

// Error satisfies the builtin error interface
func (e ArticlesDetailResponseValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sArticlesDetailResponse.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ArticlesDetailResponseValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ArticlesDetailResponseValidationError{}

// Validate checks the field values on ArticlesDetailResponse_Item with the
// rules defined in the proto definition for this message. If any rules are
// violated, an error is returned.
func (m *ArticlesDetailResponse_Item) Validate() error {
	if m == nil {
		return nil
	}

	// no validation rules for ArticleId

	if v, ok := interface{}(m.GetLabel()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return ArticlesDetailResponse_ItemValidationError{
				field:  "Label",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	if v, ok := interface{}(m.GetShop()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return ArticlesDetailResponse_ItemValidationError{
				field:  "Shop",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	if v, ok := interface{}(m.GetUser()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return ArticlesDetailResponse_ItemValidationError{
				field:  "User",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	// no validation rules for Title

	for idx, item := range m.GetResizedMainImages() {
		_, _ = idx, item

		if v, ok := interface{}(item).(interface{ Validate() error }); ok {
			if err := v.Validate(); err != nil {
				return ArticlesDetailResponse_ItemValidationError{
					field:  fmt.Sprintf("ResizedMainImages[%v]", idx),
					reason: "embedded message failed validation",
					cause:  err,
				}
			}
		}

	}

	// no validation rules for FirstPublishedAt

	// no validation rules for LatestPublishedAt

	// no validation rules for Description

	for idx, item := range m.GetTags() {
		_, _ = idx, item

		if v, ok := interface{}(item).(interface{ Validate() error }); ok {
			if err := v.Validate(); err != nil {
				return ArticlesDetailResponse_ItemValidationError{
					field:  fmt.Sprintf("Tags[%v]", idx),
					reason: "embedded message failed validation",
					cause:  err,
				}
			}
		}

	}

	for idx, item := range m.GetBlocks() {
		_, _ = idx, item

		if v, ok := interface{}(item).(interface{ Validate() error }); ok {
			if err := v.Validate(); err != nil {
				return ArticlesDetailResponse_ItemValidationError{
					field:  fmt.Sprintf("Blocks[%v]", idx),
					reason: "embedded message failed validation",
					cause:  err,
				}
			}
		}

	}

	return nil
}

// ArticlesDetailResponse_ItemValidationError is the validation error returned
// by ArticlesDetailResponse_Item.Validate if the designated constraints
// aren't met.
type ArticlesDetailResponse_ItemValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ArticlesDetailResponse_ItemValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ArticlesDetailResponse_ItemValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ArticlesDetailResponse_ItemValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ArticlesDetailResponse_ItemValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ArticlesDetailResponse_ItemValidationError) ErrorName() string {
	return "ArticlesDetailResponse_ItemValidationError"
}

// Error satisfies the builtin error interface
func (e ArticlesDetailResponse_ItemValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sArticlesDetailResponse_Item.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ArticlesDetailResponse_ItemValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ArticlesDetailResponse_ItemValidationError{}

// Validate checks the field values on ArticlesDetailResponse_Item_Label with
// the rules defined in the proto definition for this message. If any rules
// are violated, an error is returned.
func (m *ArticlesDetailResponse_Item_Label) Validate() error {
	if m == nil {
		return nil
	}

	// no validation rules for LabelId

	// no validation rules for LabelCode

	// no validation rules for Name

	// no validation rules for NameKana

	return nil
}

// ArticlesDetailResponse_Item_LabelValidationError is the validation error
// returned by ArticlesDetailResponse_Item_Label.Validate if the designated
// constraints aren't met.
type ArticlesDetailResponse_Item_LabelValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ArticlesDetailResponse_Item_LabelValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ArticlesDetailResponse_Item_LabelValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ArticlesDetailResponse_Item_LabelValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ArticlesDetailResponse_Item_LabelValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ArticlesDetailResponse_Item_LabelValidationError) ErrorName() string {
	return "ArticlesDetailResponse_Item_LabelValidationError"
}

// Error satisfies the builtin error interface
func (e ArticlesDetailResponse_Item_LabelValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sArticlesDetailResponse_Item_Label.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ArticlesDetailResponse_Item_LabelValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ArticlesDetailResponse_Item_LabelValidationError{}

// Validate checks the field values on ArticlesDetailResponse_Item_Shop with
// the rules defined in the proto definition for this message. If any rules
// are violated, an error is returned.
func (m *ArticlesDetailResponse_Item_Shop) Validate() error {
	if m == nil {
		return nil
	}

	// no validation rules for ShopId

	// no validation rules for ShopCode

	// no validation rules for Name

	// no validation rules for NameKana

	return nil
}

// ArticlesDetailResponse_Item_ShopValidationError is the validation error
// returned by ArticlesDetailResponse_Item_Shop.Validate if the designated
// constraints aren't met.
type ArticlesDetailResponse_Item_ShopValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ArticlesDetailResponse_Item_ShopValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ArticlesDetailResponse_Item_ShopValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ArticlesDetailResponse_Item_ShopValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ArticlesDetailResponse_Item_ShopValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ArticlesDetailResponse_Item_ShopValidationError) ErrorName() string {
	return "ArticlesDetailResponse_Item_ShopValidationError"
}

// Error satisfies the builtin error interface
func (e ArticlesDetailResponse_Item_ShopValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sArticlesDetailResponse_Item_Shop.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ArticlesDetailResponse_Item_ShopValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ArticlesDetailResponse_Item_ShopValidationError{}

// Validate checks the field values on ArticlesDetailResponse_Item_User with
// the rules defined in the proto definition for this message. If any rules
// are violated, an error is returned.
func (m *ArticlesDetailResponse_Item_User) Validate() error {
	if m == nil {
		return nil
	}

	// no validation rules for UserId

	// no validation rules for UserCode

	// no validation rules for Name

	// no validation rules for NameKana

	// no validation rules for Height

	// no validation rules for ImageUrl

	return nil
}

// ArticlesDetailResponse_Item_UserValidationError is the validation error
// returned by ArticlesDetailResponse_Item_User.Validate if the designated
// constraints aren't met.
type ArticlesDetailResponse_Item_UserValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ArticlesDetailResponse_Item_UserValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ArticlesDetailResponse_Item_UserValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ArticlesDetailResponse_Item_UserValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ArticlesDetailResponse_Item_UserValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ArticlesDetailResponse_Item_UserValidationError) ErrorName() string {
	return "ArticlesDetailResponse_Item_UserValidationError"
}

// Error satisfies the builtin error interface
func (e ArticlesDetailResponse_Item_UserValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sArticlesDetailResponse_Item_User.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ArticlesDetailResponse_Item_UserValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ArticlesDetailResponse_Item_UserValidationError{}

// Validate checks the field values on
// ArticlesDetailResponse_Item_ResizedMainImages with the rules defined in the
// proto definition for this message. If any rules are violated, an error is returned.
func (m *ArticlesDetailResponse_Item_ResizedMainImages) Validate() error {
	if m == nil {
		return nil
	}

	// no validation rules for Org

	// no validation rules for S

	// no validation rules for M

	// no validation rules for L

	return nil
}

// ArticlesDetailResponse_Item_ResizedMainImagesValidationError is the
// validation error returned by
// ArticlesDetailResponse_Item_ResizedMainImages.Validate if the designated
// constraints aren't met.
type ArticlesDetailResponse_Item_ResizedMainImagesValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ArticlesDetailResponse_Item_ResizedMainImagesValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ArticlesDetailResponse_Item_ResizedMainImagesValidationError) Reason() string {
	return e.reason
}

// Cause function returns cause value.
func (e ArticlesDetailResponse_Item_ResizedMainImagesValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ArticlesDetailResponse_Item_ResizedMainImagesValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ArticlesDetailResponse_Item_ResizedMainImagesValidationError) ErrorName() string {
	return "ArticlesDetailResponse_Item_ResizedMainImagesValidationError"
}

// Error satisfies the builtin error interface
func (e ArticlesDetailResponse_Item_ResizedMainImagesValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sArticlesDetailResponse_Item_ResizedMainImages.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ArticlesDetailResponse_Item_ResizedMainImagesValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ArticlesDetailResponse_Item_ResizedMainImagesValidationError{}

// Validate checks the field values on ArticlesDetailResponse_Item_Tags with
// the rules defined in the proto definition for this message. If any rules
// are violated, an error is returned.
func (m *ArticlesDetailResponse_Item_Tags) Validate() error {
	if m == nil {
		return nil
	}

	// no validation rules for TagId

	// no validation rules for Name

	return nil
}

// ArticlesDetailResponse_Item_TagsValidationError is the validation error
// returned by ArticlesDetailResponse_Item_Tags.Validate if the designated
// constraints aren't met.
type ArticlesDetailResponse_Item_TagsValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ArticlesDetailResponse_Item_TagsValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ArticlesDetailResponse_Item_TagsValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ArticlesDetailResponse_Item_TagsValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ArticlesDetailResponse_Item_TagsValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ArticlesDetailResponse_Item_TagsValidationError) ErrorName() string {
	return "ArticlesDetailResponse_Item_TagsValidationError"
}

// Error satisfies the builtin error interface
func (e ArticlesDetailResponse_Item_TagsValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sArticlesDetailResponse_Item_Tags.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ArticlesDetailResponse_Item_TagsValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ArticlesDetailResponse_Item_TagsValidationError{}

// Validate checks the field values on ArticlesDetailResponse_Item_Blocks with
// the rules defined in the proto definition for this message. If any rules
// are violated, an error is returned.
func (m *ArticlesDetailResponse_Item_Blocks) Validate() error {
	if m == nil {
		return nil
	}

	// no validation rules for SortNum

	// no validation rules for ClientHtml

	return nil
}

// ArticlesDetailResponse_Item_BlocksValidationError is the validation error
// returned by ArticlesDetailResponse_Item_Blocks.Validate if the designated
// constraints aren't met.
type ArticlesDetailResponse_Item_BlocksValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ArticlesDetailResponse_Item_BlocksValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ArticlesDetailResponse_Item_BlocksValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ArticlesDetailResponse_Item_BlocksValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ArticlesDetailResponse_Item_BlocksValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ArticlesDetailResponse_Item_BlocksValidationError) ErrorName() string {
	return "ArticlesDetailResponse_Item_BlocksValidationError"
}

// Error satisfies the builtin error interface
func (e ArticlesDetailResponse_Item_BlocksValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sArticlesDetailResponse_Item_Blocks.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ArticlesDetailResponse_Item_BlocksValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ArticlesDetailResponse_Item_BlocksValidationError{}
