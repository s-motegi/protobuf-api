package usrattval

import (
	"bitbucket.org/s-motegi/protobuf-api/adapter/database"
	domain "bitbucket.org/s-motegi/protobuf-api/domain/user"
	"bitbucket.org/s-motegi/protobuf-api/usecase/user_attribute_values"
)

// Repository ...
type Repository struct {
	database.SQLHandler
}

// NewRepository ...
func NewRepository(h database.SQLHandler) usrattval.Repository {
	return &Repository{h}
}

// FindByID ...
func (repo *Repository) FindByID(id int) (*domain.UserAttributeValues, error) {
	vals := &domain.UserAttributeValues{}
	if err := repo.SQLHandler.Get(vals, "SELECT * FROM user_attribute_values WHERE id = ?", id); err != nil {
		return nil, err
	}

	return vals, nil
}
